import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class LogInTest {


    public static WebDriver driver;

    @BeforeClass
    public void SetUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/");
    }

    @Test
    public void LogInTest() {

        WebElement myaccount = driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]"));
        myaccount.click();
        WebElement login = driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a"));
        login.click();
        String title = driver.getTitle();
        Assert.assertEquals(title, "Account Login");

        WebElement username=driver.findElement(By.xpath("//*[@id=\"input-email\"]"));
        username.sendKeys("man4oni@mail.ru");

        WebElement pass=driver.findElement(By.xpath("//*[@id=\"input-password\"]"));
        pass.sendKeys("12345678");

        WebElement loginBut=driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input"));
        loginBut.click();
        String my_title=driver.getTitle();
        Assert.assertEquals(my_title,"My Account");

    }
}
