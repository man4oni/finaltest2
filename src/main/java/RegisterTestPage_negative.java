import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class RegisterTestPage_negative {
    public static WebDriver driver;

    @BeforeClass
    public void SetUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/");
    }

    @Test
    public void LogInTest() {

        WebElement myaccount = driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]"));
        myaccount.click();
        WebElement register = driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[1]/a"));
        register.click();
        String title = driver.getTitle();
        Assert.assertEquals(title,"Register Account");

        WebElement continueBut = driver.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[2]"));
        continueBut.click();

        WebElement warningVerify=driver.findElement(By.xpath("//*[@id=\"account-register\"]/div[1]"));
        String warning=warningVerify.getText();
        Assert.assertEquals(warning,"Warning: You must agree to the Privacy Policy!");

        WebElement firstNameVerify=driver.findElement(By.xpath("//*[@id=\"account\"]/div[2]/div/div"));
        String firstName=firstNameVerify.getText();
        Assert.assertEquals(firstName,"First Name must be between 1 and 32 characters!");

        WebElement lastNameVerify=driver.findElement(By.xpath("//*[@id=\"account\"]/div[3]/div/div"));
        String lastName=lastNameVerify.getText();
        Assert.assertEquals(lastName,"Last Name must be between 1 and 32 characters!");

        WebElement emailVerify=driver.findElement(By.xpath("//*[@id=\"account\"]/div[4]/div/div"));
        String email=emailVerify.getText();
        Assert.assertEquals(email,"E-Mail Address does not appear to be valid!");

        WebElement phoneVerify=driver.findElement(By.xpath("//*[@id=\"account\"]/div[5]/div/div"));
        String phone=phoneVerify.getText();
        Assert.assertEquals(phone,"Telephone must be between 3 and 32 characters!");

        WebElement passVerify=driver.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset[2]/div[1]/div/div"));
        String pass=passVerify.getText();
        Assert.assertEquals(pass,"Password must be between 4 and 20 characters!");



    }

    @AfterClass
    public void ipochvamdabegambate() {
        driver.quit();

    }
}
