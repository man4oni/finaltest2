import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class RegisterTestPage_positive {


    public static WebDriver driver;

    @BeforeClass
    public void SetUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/");
    }

    @Test
    public void LogInTest() {

        WebElement myaccount = driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]"));
        myaccount.click();
        WebElement register = driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[1]/a"));
        register.click();
        String title = driver.getTitle();
        Assert.assertEquals(title,"Register Account");

        WebElement firstName = driver.findElement(By.xpath("//*[@id=\"input-firstname\"]"));
        firstName.sendKeys("ivan");
        WebElement lastName = driver.findElement(By.xpath("//*[@id=\"input-lastname\"]"));
        lastName.sendKeys("vancho");
        WebElement email = driver.findElement(By.xpath("//*[@id=\"input-email\"]"));
        email.sendKeys("ivan@gmail.com");
        WebElement phone = driver.findElement(By.xpath("//*[@id=\"input-telephone\"]"));
        phone.sendKeys("0888123456");
        WebElement pass = driver.findElement(By.xpath("//*[@id=\"input-password\"]"));
        pass.sendKeys("12345678");
        WebElement passConfirm = driver.findElement(By.xpath("//*[@id=\"input-confirm\"]"));
        passConfirm.sendKeys("12345678");
        WebElement subscribe = driver.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset[3]/div/div/label[1]/input"));
        subscribe.click();
        if (!subscribe.isSelected()) {
            subscribe.click();
        }

        assertTrue(subscribe.isSelected());
        WebElement agree = driver.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[1]"));
        agree.click();
        WebElement continueBut = driver.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[2]"));
        continueBut.click();

        String title1= driver.getTitle();
        Assert.assertEquals(title1,"Your Account Has Been Created!");



    }

    @AfterClass
    public void ipochvamdabegambate() {
        driver.quit();

    }
}